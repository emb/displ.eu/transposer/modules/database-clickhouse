# Transposer Clickhouse Database

Database abstraction for Clickhouse using SQLAlchemy
The abstraction offers a high-level interface that homogenize the use of different databases as much as possible.

**Be aware that the clickhouse driver is not compatible with current *SQLAlchemy* versions.**<br>
**clickhouse_sqlalchemy requires sqlalchemy >=1.4.24 & <1.5**

## Install

To install, use the *Shared Library (shared-lib)* template project and clone this one into the `modules` directory. If you follow the installation instructions of the service(s) you intend to use and `shared-lib`, the service(s) will pick up the module automatically.

### Dependencies

To install the dependencies,
```bash
pip install -r requirements.txt
```
inside each service's virtual environment that uses the `shared-lib`.

